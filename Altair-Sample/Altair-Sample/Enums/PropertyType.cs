﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Altair_Sample.Enums
{
    public enum PropertyType
    {
        HOTEL, HOSTEL, CAMPING, APARTAMENT
    }
}
