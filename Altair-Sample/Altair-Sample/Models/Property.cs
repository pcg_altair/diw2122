﻿using Altair_Sample.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Altair_Sample.Models
{
    public class Property
    {
        public int ID { get; set; }
        [Required]
        [StringLength(200, ErrorMessage ="This field must be min 5 character and max 200 character", MinimumLength = 5)]
        public string Addres { get; set; }
        [Required]
        public string Country { get; set; }
        [RegularExpression(@"^[0-9]{5}$")]
        public string PostalCode { get; set; }
        /*
         [Range(0,99999)]
         private int PostalCode {get; set;}
         */
        [Required]
        public PropertyType PropertyType { get; set; }

        [ForeignKey("LandlordId")]
        public Landlord Landlord { get; set; }
        [InverseProperty("Property")]
        public List<Booking> Bookings { get; set; }
    }
}
