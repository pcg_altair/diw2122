﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Altair_Sample.Models
{
    public class Customer: Person
    {
        public string CreditCardName { get; set; }
        public string CreditCardNumber { get; set; }
        public int Cvv { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }

        //@OneToMany(mappedBy = "Customer")
        [InverseProperty("Customer")]
        public List<Booking> Bookings { get; set; }
    }
}
