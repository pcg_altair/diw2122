﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Altair_Sample.Models
{
    public class Landlord: Person
    {
        public string Passport { get; set; }
        [InverseProperty("Landlord")]
        public List<Property> Properties { get; set; }
    }
}
