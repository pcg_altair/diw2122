﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Altair_Sample.Models
{
    [Table("BOOKING")]
    public class Booking
    {
        public int ID { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        //@ManyToOne(fetch = FetchType.EAGER, optional = false)
        //@JoinColum(name = "customer_id")
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [ForeignKey("PropertyId")]
        public Property Property { get; set; }
    }
}
