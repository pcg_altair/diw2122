﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Altair_Sample.Models
{
    public abstract class Person
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        [NotMapped]
        public string FullName { get => Name + " " + Surname; }
    }
}
