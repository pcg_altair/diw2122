﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Altair_Sample.Models;

#nullable disable

namespace Altair_Sample.Data
{
    public partial class AltairSampleContext : DbContext
    {
        public AltairSampleContext()
        {
        }

        public AltairSampleContext(DbContextOptions<AltairSampleContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Cargar en la variable configuration los datos del JSON
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
                
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            }
        }

        // En este método tenemos que agregar las classes que quiero mapera en BBDD
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");
            modelBuilder.Entity<Customer>().ToTable("CUSTOMER");
            modelBuilder.Entity<Landlord>().ToTable("LANDLORD");
            modelBuilder.Entity<Booking>().ToTable("BOOKING");
            modelBuilder.Entity<Property>().ToTable("PROPERTY");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<Altair_Sample.Models.Property> Property { get; set; }

        public DbSet<Altair_Sample.Models.Customer> Customer { get; set; }

        public DbSet<Altair_Sample.Models.Landlord> Landlord { get; set; }
    }
}
